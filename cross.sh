CI_REGISTRY=registry.freedesktop.org
CI_REGISTRY_IMAGE=$CI_REGISTRY/fahien/ubuntu-image

docker login $CI_REGISTRY
docker buildx create --name cross-builder --use
docker buildx inspect --bootstrap
docker buildx build --platform linux/amd64,linux/arm64 -t $CI_REGISTRY_IMAGE:latest --push .
