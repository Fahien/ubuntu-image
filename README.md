# Dependencies

1. Install docker and qemu-user-static to be able to build for arm64. Do not install it from snap, or it would not be able to use `docker-credential-secretservice`.

```sh
sudo apt install docker.io qemu-user-static
```

2. Install docker buildx following [this procedure](https://github.com/docker/buildx/#installing)

```sh
VERSION=0.3.1
TARGET=linux
ARCH=amd64
BUILDX=buildx-v$VERSION.$TARGET-$ARCH
wget https://github.com/docker/buildx/releases/v$VERSION/$BUILDX
chmod a+x $BUILDX
mv $BUILDX ~/.docker/cli-plugins/docker-buildx
```

3. To run docker as non-root user, create a docker group and add your user.

```sh
groupadd docker
usermod -aG docker ${USER}
newgrp docker
chmod 666 /var/run/docker.sock
docker info
```

4. Configure a credential helper, like docker-credential-secretservice (I have made a snap for it):

```sh
echo '{ "credsStore": "secretservice" }' > ~/.docker/config.json
```
